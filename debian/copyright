Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PyNAC
Upstream-Contact: Burcin Erocal <burcin@erocal.org>
Source: http://pynac.org

Files: *
Copyright: 2012 Burcin Erocal
License: GPL-2+

Files: m4/ax_cxx_compile_stdcxx.m4
Copyright: 2008 Benjamin Kosnik <bkoz@redhat.com>
           2012 Zack Weinberg <zackw@panix.com>
	   2013 Roy Stogner <roystgnr@ices.utexas.edu>
	   2014, 2015 Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
	   2015 Paul Norman <penorman@mac.com>
	   2015 Moritz Klammler <moritz@klammler.eu>
	   2016 Krzesimir Nowak <qdlacz@gmail.com>
License: FSFAP

Files: m4/ax_prefix_config_h.m4
Copyright: 2014 Reuben Thomas <rrt@sc3d.org>
           2008 Guido U. Draheim <guidod@gmx.de>
	   2008 Marten Svantesson
	   2008 Gerald Point <Gerald.Point@labri.fr>
License: GPL-3+

Files: m4/ax_python_devel.m4
Copyright: 2009 Sebastian Huber <sebastian-huber@web.de>
           2009 Alan W. Irwin
	   2009 Rafael Laboissiere <rafael@laboissiere.net>
	   2009 Andrew Collier
	   2009 Matteo Settenvini <matteo@member.fsf.org>
	   2009 Horst Knorr <hk_classes@knoda.org>
	   2013 Daniel Mullner <muellner@math.stanford.edu>
License: GPL-3+

Files: m4/pkg.m4
Copyright: 2004 Scott James Remnant <scott@netsplit.com>
License: GPL-2+

Files: ginac/*
Copyright: 1999-2008, Johannes Gutenberg University Mainz, Germany
License: GPL-2+

files: ginac/order.*
Copyright: 2011 Burcin Erocal and Jean-Pierre Flori
License: GPL-2+

Files: ginac/infinity.*
Copyright: 2011, Volker Braun <vbraun@stp.dias.ie>
License: GPL-2+

Files: ginac/numeric.h
Copyright: 1999-2008 Johannes Gutenberg University Mainz, Germany
	   2008 William Stein
	   2015-2016 Ralf Stephan <ralf@ark.in-berlin.de>
License: GPL-2+

Files: ginac/numeric.cpp
Copyright: 1999-2008 Johannes Gutenberg University Mainz, Germany
	   2008 William Stein
	   2009 Burcin Erocal <burcin@erocal.org>
	   2015-2017 Ralf Stephan <ralf@ark.in-berlin.de>
License: GPL-2+

Files: ginac/infoflagbase.*
       ginac/assume.*
       ginac/context.*
       ginac/inifcns_orthopoly.cpp
       ginac/function_info.cpp
       ginac/sum.*
       ginac/useries.*
       ginac/useries-flint.h
Copyright: 2015-2017 Ralf Stephan <ralf@ark.in-berlin.de>
License: GPL-2+

Files: ginac/inifcns_hyperb.cpp
       ginac/inifcns_hyperg.cpp
       ginac/inifcns_comb.cpp
       ginac/mpoly.*
       ginac/mpoly-giac.cpp
       ginac/mpoly-ginac.cpp
       ginac/mpoly-singular.cpp
       ginac/upoly.h
       ginac/upoly-ginac.cpp
Copyright: 1999-2008 Johannes Gutenberg University Mainz, Germany
           2016 Ralf Stephan <ralf@ark.in-berlin.de>
License: GPL-2+

Files: debian/*
Copyright: 2012-2018, Julien Puydt <jpuydt@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-3'

License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.
