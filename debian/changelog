pynac (0.7.29-2) unstable; urgency=medium

  * Team upload.
  * Add realpartloop.patch from sagemath.
  * Rebuild against singular 4.2.1-p2.

 -- Tobias Hansen <thansen@debian.org>  Sun, 12 Dec 2021 11:08:41 +0000

pynac (0.7.29-1) unstable; urgency=medium

  * Fix d/watch.
  * Bump standards-version to 4.6.0.
  * New upstream release.
  * Drop patch (upstreamed even though Debian-specific!).

 -- Julien Puydt <jpuydt@debian.org>  Sat, 28 Aug 2021 22:32:08 +0200

pynac (0.7.27-1) unstable; urgency=medium

  * New upstream release.
  * Update patches (dropping upstreamed one).
  * Bump standards-version to 4.5.1.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 29 Jan 2021 08:32:10 +0100

pynac (0.7.26-5) unstable; urgency=medium

  [ Tobias Hansen ]
  * Add py_ssize_t_clean.patch to fix deprecation warnings
    with Python 3.8.

  [ Julien Puydt ]
  * Bump std-vers to 4.5.0.
  * Bump dh compat to 13.
  * Declare d/rules doesn't require root.
  * Document find_singular.patch isn't forwarded for good reasons.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 24 Aug 2020 12:29:43 +0200

pynac (0.7.26-4) unstable; urgency=medium

  * Make libpynac18py3 conflict with libpynac18.

 -- Julien Puydt <jpuydt@debian.org>  Thu, 12 Sep 2019 07:53:50 +0200

pynac (0.7.26-3) unstable; urgency=medium

  * Fix the switch to Python3 (Closes: #937486).

 -- Julien Puydt <jpuydt@debian.org>  Wed, 11 Sep 2019 11:48:51 +0200

pynac (0.7.26-2) experimental; urgency=medium

  * Switch to Python 3, hence change the library package name
   because it's incompatible - we'll change back when upstream
   bumps the soname.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 09 Sep 2019 15:28:47 +0200

pynac (0.7.26-1) experimental; urgency=medium

  * New upstream release.
  * Remove upstreamed patch (lintian warnings for pkgconfig).
  * Drop d/compat and switch to debhelper compat 12.
  * Bum std-ver to 4.4.0.

 -- Julien Puydt <jpuydt@debian.org>  Sat, 31 Aug 2019 08:53:31 +0200

pynac (0.7.25-1) experimental; urgency=medium

  * New upstream release.
  * Add patch to fix lintian warnings.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 12 May 2019 18:20:56 +0200

pynac (0.7.24-1) experimental; urgency=medium

  * New upstream release.

 -- Julien Puydt <jpuydt@debian.org>  Sun, 03 Mar 2019 14:31:33 +0100

pynac (0.7.23-2) unstable; urgency=medium

  * Team upload to unstable.

 -- Tobias Hansen <thansen@debian.org>  Tue, 15 Jan 2019 13:54:40 +0100

pynac (0.7.23-1) experimental; urgency=medium

  * New upstream release.
  * Bump std-ver to 4.3.0.

 -- Julien Puydt <jpuydt@debian.org>  Mon, 07 Jan 2019 00:03:18 +0100

pynac (0.7.22-4) unstable; urgency=medium

  * Team upload.
  * Rebuild against singular 4.1.1-p2+ds-3.

 -- Tobias Hansen <thansen@debian.org>  Wed, 31 Oct 2018 23:06:54 +0100

pynac (0.7.22-3) unstable; urgency=medium

  * Team upload.
  * Rebuild against singular 4.1.1-p2.

 -- Tobias Hansen <thansen@debian.org>  Sat, 06 Oct 2018 14:56:28 +0200

pynac (0.7.22-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Tobias Hansen <thansen@debian.org>  Wed, 26 Sep 2018 20:15:34 +0200

pynac (0.7.22-1) experimental; urgency=medium

  * New upstream release.
  * Bump soname to 18.
  * Bump std-ver to 4.1.5.

 -- Julien Puydt <jpuydt@debian.org>  Fri, 06 Jul 2018 23:01:34 +0200

pynac (0.7.19-2) unstable; urgency=medium

  * Team upload to unstable.

 -- Tobias Hansen <thansen@debian.org>  Tue, 26 Jun 2018 11:09:32 +0200

pynac (0.7.19-1) experimental; urgency=medium

  * New upstream release.

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 13 Apr 2018 08:40:02 +0200

pynac (0.7.18-1) experimental; urgency=medium

  * New upstream release.

 -- Julien Puydt <julien.puydt@laposte.net>  Sun, 25 Mar 2018 15:47:30 +0200

pynac (0.7.14-1) experimental; urgency=medium

  * Team upload.

  [ Julien Puydt ]
  * New upstream release.
  * Bump soname version to 17.

  [ Tobias Hansen ]
  * Update Vcs-* fields after migration to salsa.

 -- Tobias Hansen <thansen@debian.org>  Sun, 07 Jan 2018 18:58:31 +0100

pynac (0.7.12-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Tobias Hansen <thansen@debian.org>  Sat, 30 Dec 2017 10:14:07 +0100

pynac (0.7.12-1) experimental; urgency=medium

  * Team upload.

  [ Julien Puydt ]
  * New upstream release.
  * Bump soname version to 15.
  * Bump d/watch version to 4.
  * Bump std-ver to 4.1.1.

 -- Tobias Hansen <thansen@debian.org>  Sun, 15 Oct 2017 20:25:17 +0100

pynac (0.7.10-1) experimental; urgency=medium

  * New upstream release.
  * Drop static-python-fix.patch, which got upstreamed.
  * Bump standards version to 4.0.0.

 -- Julien Puydt <julien.puydt@laposte.net>  Tue, 15 Aug 2017 00:26:56 +0200

pynac (0.7.8-3) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Ximin Luo <infinity0@debian.org>  Fri, 18 Aug 2017 14:26:08 +0200

pynac (0.7.8-2) experimental; urgency=medium

  * Team upload.
  * Apply static-python-fix.patch, fixing a segfault after the docbuild
    of sagemath.

 -- Tobias Hansen <thansen@debian.org>  Wed, 21 Jun 2017 16:09:48 +0100

pynac (0.7.8-1) experimental; urgency=medium

  * Updated to latest upstream.
  * Bumped soname version up to 13.

 -- Julien Puydt <julien.puydt@laposte.net>  Mon, 05 Jun 2017 15:06:55 +0200

pynac (0.7.5-1) experimental; urgency=medium

  [ Julien Puydt ]
  * Updated to latest upstream.
  * Bumped soname version up to 10.

  [ Tobias Hansen ]
  * Build-Depend on libsingular4-dev (>= 1:4.1.0-p2) to ensure the package
    from experimental is used.

 -- Julien Puydt <julien.puydt@laposte.net>  Thu, 16 Feb 2017 08:02:36 +0100

pynac (0.7.4-1) experimental; urgency=medium

  * Updated to latest upstream.
  * Added build-dep on flint.
  * Bumped soname version up to 9.
  * Added libsingular4-dev, libntl-dev and pkg-config as build-dep.
  * Added a patch to find the singular libs.

 -- Julien Puydt <julien.puydt@laposte.net>  Sun, 29 Jan 2017 21:14:58 +0100

pynac (0.6.91-1) unstable; urgency=medium

  * Updated to latest upstream.
  * Pushed dh compat to 10.
  * Marked the -dev package as Multi-Arch:same as hinted in the tracker.

 -- Julien Puydt <julien.puydt@laposte.net>  Thu, 06 Oct 2016 07:51:24 +0200

pynac (0.6.9-1) unstable; urgency=medium

  * Updated to latest upstream.

 -- Julien Puydt <julien.puydt@laposte.net>  Sat, 03 Sep 2016 12:35:58 +0200

pynac (0.6.8-1) unstable; urgency=medium

  * Updated to latest upstream.

 -- Julien Puydt <julien.puydt@laposte.net>  Tue, 19 Jul 2016 19:01:54 +0200

pynac (0.6.7-1) unstable; urgency=medium

  * Updated to latest upstream.

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 10 Jun 2016 15:14:41 +0200

pynac (0.6.5-1) unstable; urgency=medium

  * Updated to latest upstream.
  * Bump the standards-version.
  * Use https in Vcs-* fields.
  * Update copyrights

 -- Julien Puydt <julien.puydt@laposte.net>  Thu, 12 May 2016 13:20:59 +0200

pynac (0.6.0-1) unstable; urgency=medium

  * Updated to latest upstream.
  * Follow the soname bump (libpynac1 -> libpynac2).

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 08 Jan 2016 17:43:38 +0100

pynac (0.5.4-1) unstable; urgency=medium

  * Update to latest upstream.
  * Follow the soname bump (libpynac0 -> libpynac1).
  * Point Vcs-Browser to cgit.
  * Stop mixing spaces and tabs in d/copyright.
  * Update my copyright up to 2016 in d/copyright.

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 01 Jan 2016 19:26:27 +0100

pynac (0.5.0-1) unstable; urgency=medium

  * Updated to latest upstream.
  * Put the library package in section libs.
  * Changed the homepage, since it moved.
  * Added a build-dep on libgmp-dev.

 -- Julien Puydt <julien.puydt@laposte.net>  Tue, 03 Nov 2015 08:29:11 +0100

pynac (0.4.3-1) unstable; urgency=medium

  * Updated to latest upstream.

 -- Julien Puydt <julien.puydt@laposte.net>  Thu, 13 Aug 2015 22:40:16 +0200

pynac (0.3.7-1) unstable; urgency=medium

  * Updated to latest upstream (Closes: #722820).
  * Removed all patches (upstream applied them).
  * Bumped standards up.
  * Updated the copyright information.

 -- Julien Puydt <julien.puydt@laposte.net>  Thu, 07 May 2015 06:51:41 +0200

pynac (0.3.2+dfsg-1) unstable; urgency=low

  * Updated to latest upstream (Closes: #710635).
  * Added a patch to fix the libtool versioning (the current package has
    a lower soname than the previous one, but from 0.3.3 onwards, upstream
    has accepted that patch so that shouldn't happen again hopefully).
  * Updated d/watch and d/copyright to find new upstream release and repack
    without mercurial files.

 -- Julien Puydt <julien.puydt@laposte.net>  Sun, 18 May 2014 10:47:35 +0200

pynac (0.2.6-1) unstable; urgency=low

  * Initial release (Closes: #678332).
  * Patch to remove obsolete comments (sent upstream).

 -- Julien Puydt <julien.puydt@laposte.net>  Fri, 22 Mar 2013 15:05:37 +0100
